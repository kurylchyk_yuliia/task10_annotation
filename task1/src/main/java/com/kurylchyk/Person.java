package com.kurylchyk;

import org.apache.logging.log4j.*;


@CustomAnnotation(name = "Yullia Kurylchyk", value = 19)
public class Person<T> {

    @CustomAnnotation(name = "Yullia")
   private String name;
    private  String surname;
    @CustomAnnotation(name = "Yuliia Kurylchyk", value = 19)
    private int age;
    private T someInfo;
    private Logger logger = LogManager.getLogger(Person.class);


    public Person() {

    }

    @CustomAnnotation(name = "Yulia")
    public void getName(String name) {
        logger.info("Name: " + name);
    }

    @CustomAnnotation(name = "Kurylchyk")
    public void getSurname(String name, String surname) {
        logger.info("Name and surname: "+name +"\t"+ surname);
    }

    @CustomAnnotation(name="Yullia Kurylchyk",value = 19)
    public void getAge(int age) {
        logger.info("Age: "+ age);
    }

    public void getSomeInfo(){

        logger.info(someInfo);
    }

    public void myMethod(String a, int ... args){

        logger.info("");
        logger.info("%nString :%s",a);
        for(int element:args){
            logger.info("\t"+element);
        }
    }

    public void myMethod(String...args) {

        logger.info("");
        for(String element: args){
            logger.info(element+"\t");
        }
    }
}
