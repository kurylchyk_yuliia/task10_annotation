package com.kurylchyk;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.*;
import org.apache.logging.log4j.*;

@CustomAnnotation
public class UnknownType<T> {

    private T object;
    private final Class<T> clazz;
    private Logger logger = LogManager.getLogger(UnknownType.class);
    private View view;

    public UnknownType(Class<T> clazz){
        view =(s)->logger.info(s);
        view.show("UNKNOWN TYPE");
        this.clazz = clazz;
        try{
            object = clazz.getConstructor().newInstance();
        }catch(NoSuchMethodException e){
            System.out.println(e.getMessage());
        } catch(IllegalAccessException e) {
            System.out.println(e.getMessage());
        } catch (InstantiationException e) {
            System.out.println(e.getMessage());
        } catch (InvocationTargetException e) {
            System.out.println(e.getMessage());
        }
    }


    public void getMethods(){
        view.show("\nAll declared methods");
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for(Method element: declaredMethods)
            view.show(element.toString());
    }

    public void getName(){

        view.show("Name of class" + clazz.getName());
    }

    public void getFields(){

        view.show("\nAll declared fields");
        Field[] declaredFields = clazz.getDeclaredFields();
        for(Field element: declaredFields)
            view.show(element.toString());
    }

    public void getPackage(){
        view.show("Package name: " + clazz.getPackage());
    }

    public void getConstructor(){
        view.show("All declared constructors");
        Constructor[] declaredConstructor = clazz.getDeclaredConstructors();

        for(Constructor element:declaredConstructor){
            view.show(element.toString());
        }
    }
}
