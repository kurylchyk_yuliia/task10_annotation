package com.kurylchyk;

import org.apache.logging.log4j.*;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.Scanner;

public class WorkWithAnnotation {

    private Class<Person> obj;
    private Person person;
    private Annotation annotation;
    private Logger logger = LogManager.getLogger(WorkWithAnnotation.class);
    private View view;

    WorkWithAnnotation() {
        view = (s) -> logger.info(s);
        view.show("WORK WITH ANNOTATION");
        obj = Person.class;
        annotation = obj.getAnnotation(CustomAnnotation.class);
        person = new Person();
    }

    public void showAnnotation() {
        if (obj.isAnnotationPresent(CustomAnnotation.class)) {

            view.show("\nAnnotation value");
            CustomAnnotation myCustomAnnotation = (CustomAnnotation) annotation;
            view.show("\nName of class: "+ obj.getSimpleName());
            view.show("\nName :"+ myCustomAnnotation.name());
            view.show("\nAge :"+ myCustomAnnotation.value());
        }
    }


    public void showFields() {

        view.show("\nAll declared fields");
        Field[] fields = obj.getDeclaredFields();
        for (Field element : fields) {
            if (element.isAnnotationPresent(CustomAnnotation.class)) {
                view.show(element.toString());
            } else continue;
        }
    }


    public void showMethods() {
        view.show("\nAll available methods");
        Method[] declaredMethods = obj.getDeclaredMethods();
        for (Method element : declaredMethods)
            view.show(element.toString());
    }

    public void dealWithMethod() {
        try {
            Method accessor = obj.getDeclaredMethod("getName", String.class);
            accessor.invoke(person, "Iryna");
            accessor = obj.getDeclaredMethod("getSurname", String.class, String.class);
            accessor.invoke(person, "Iryna", "Sholop");
            accessor = obj.getDeclaredMethod("getAge", int.class);
            accessor.invoke(person, 18);

        } catch (NoSuchMethodException ex) {
            view.show("Unknown method");
        } catch (InvocationTargetException | IllegalAccessException e) {
            System.out.println(e.getMessage());
        }
    }

    public <T> void setName(T type) {


        Class<?> clazz = person.getClass();
        if (clazz != null) {
            try {
                Field field = clazz.getDeclaredField("someInfo");
                field.setAccessible(true);
                field.set(person, type);
                person.getSomeInfo();
            } catch (NoSuchFieldException e) {
                clazz = clazz.getSuperclass();
            } catch (Exception e) {
                throw new IllegalStateException(e);
            }

        }


    }


    public void setStringAndInt(String str, int... args) {

        try {
            Method oneStringManyInt = obj.getDeclaredMethod("myMethod", new Class[]{String.class, int[].class});
            oneStringManyInt.invoke(person, str, args);
        } catch (NoSuchMethodException e) {
            System.out.println(e.getMessage());
        } catch (InvocationTargetException | IllegalAccessException e) {
            System.out.println(e.getMessage());
        }
    }

    public void setArrayString(String... args) {

        try {
            Method methodString = obj.getDeclaredMethod("myMethod", new Class[]{String[].class});
            methodString.invoke(person, new Object[]{args});
        } catch (NoSuchMethodException e) {
            System.out.println(e.getMessage());
        } catch (InvocationTargetException | IllegalAccessException e) {
            System.out.println(e.getMessage());
        }
    }
}