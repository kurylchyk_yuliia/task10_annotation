package com.kurylchyk;

public class Application {

    public static void main(String[] args) {

        WorkWithAnnotation workWithAnnotation =   new WorkWithAnnotation();

        workWithAnnotation.setName("Hello");
        workWithAnnotation.setStringAndInt("java",new int[]{1,5,3,87,53,22});
        workWithAnnotation.setArrayString(new String[]{"hello","my","friends"});

        UnknownType<WorkWithAnnotation> unknownType = new UnknownType<>(WorkWithAnnotation.class);
        unknownType.getFields();
        unknownType.getName();
        unknownType.getConstructor();
    }


}

