package com.kurylchyk;
import java.lang.annotation.*;


@Retention(RetentionPolicy.RUNTIME)
public @interface CustomAnnotation {

    String name() default "";
    int value() default 0;

}
